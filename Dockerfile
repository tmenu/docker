FROM debian:jessie-backports

MAINTAINER Thomas Menu <menu.tho@gmail.com>

ENV TERM xterm

RUN systemMods=" \
        net-tools \
        vim \
        dialog \
        apt-utils \
        xterm \
        man-db \
        manpages-fr \
        curl \
        wget \
        openssl \
        acl \
        htop \
        git \
        graphicsmagick \
        python-software-properties \
        apache2 \
        apache2-utils \
    " \
    && apt-get update \
    && apt-get install -y $systemMods \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Create default apache2 conf
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Enabled apache2 modules
RUN a2enmod rewrite \
    && a2enmod headers \
    && a2enmod ssl

# Dot files
COPY ./dot/.bash_aliases /root/.bash_aliases
RUN echo "source /root/.bash_aliases" >> /root/.bashrc

COPY ./dot/.git_aliases /root/.git_aliases
RUN chmod +x /root/.git_aliases
RUN /root/.git_aliases

# ADD Docker User
RUN addgroup --gid=1000 docker \
    && adduser --system --uid=1000 --shell /bin/bash docker \
    && echo "docker ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# CMD script
COPY ./run.sh /run.sh
RUN chmod +x /run.sh

WORKDIR /var/www/html