# Docker base images #
- - -

### [**apache2.2/**](https://bitbucket.org/tmenu/docker/src/HEAD/apache2.2/?at=master) :
* Debian Wheezy
* Apache 2.2
- - -

### [**apache2.2-php5.4/**](https://bitbucket.org/tmenu/docker/src/HEAD/apache2.2-php5.4/?at=master) :
* Debian Wheezy
* Apache 2.2
* PHP 5.4
- - -

### [**apache2.4/**](https://bitbucket.org/tmenu/docker/src/HEAD/apache2.4/?at=master) :
* Debian Jessie
* Apache 2.4
- - -

### [**apache2.4-php5.6/**](https://bitbucket.org/tmenu/docker/src/HEAD/apache2.4-php5.6/?at=master) :
* Debian Jessie
* Apache 2.4
* PHP 5.6
- - -

### [**project/**](https://bitbucket.org/tmenu/docker/src/HEAD/project/?at=master) :
* Test webserver
* [**project/docker/install.md**](https://bitbucket.org/tmenu/docker/src/HEAD/project/docker/install.md?at=master&fileviewer=file-view-default)
- - -
