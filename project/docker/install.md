# WEBSERVER install on dev environment #
--------

This tutorial is going to describe the step by step installation for the WEBSERVER global environment:

## Install Docker

The project is running on a virtual environment to be production ready

### Linux ###

    sudo apt-get update
    sudo apt-get install wget
    sudo apt-get install curl
    wget -qO- https://get.docker.com/ | sh

Note: If your company is behind a filtering proxy, you may find that the apt-key command fails for the Docker repo during installation. To work around this, add the key directly using the following:

    wget -qO- https://get.docker.com/gpg | sudo apt-key add -

Create docker group

    sudo usermod -aG docker <USER>

Install docker compose

    curl -L https://github.com/docker/compose/releases/download/1.5.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose

### Mac OS ###

    download https://www.docker.com/docker-toolbox (mac version)
    install the package
    open a terminal

type the commands

    docker-machine create default --driver=virtualbox
    eval $(docker-machine env default)

### Windows ###

    download https://www.docker.com/docker-toolbox (windows version)
    install the package
    open a terminal (e.g. http://cmder.net/)

type the commands

    docker-machine create default --driver=virtualbox
    eval $(docker-machine env default)

## Clone the front repository
In the parent directory of the current directory run

    none

## Launch the box
Go to the directory _init/docker and run docker-compose

    cd <git-repos>/project/docker/
    docker-compose up -d

## log into app container and run composer

    docker exec -it app_webserver /bin/bash

answer to composer final questions :

    database host: webserver_db
    database name: WEBSERVER
    database user: webserver
    database password: webserver

Run last commands

    N/A

## Override the dns redirection
In the `/etc/hosts` file of your computer add the following lines :

    127.0.0.1 webserver.local

for MacOs and Windows the IP is not 127.0.0.1 but the IP obtained by the command

    docker-machine ip default

## URL

    http://webserver.local/             Web access (directory: public/)
    http://webserver.local:8080/        PHPMyAdmin
    http://webserver.local:5001/        DockerUI
