<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Test MySQL connection</title>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php

    $host = 'webserver_db';
    $user = 'webserver';
    $pass = 'webserver';
    $base = 'WEBSERVER';

    $queries = array(
        'SHOW DATABASES',
        'SELECT * FROM user'
    );

    $db = new PDO('mysql:host=' . $host . ';dbname=' . $base . ';charset=utf8mb4', $user, $pass);


    foreach ($queries as $query) {
        var_dump($query);

        try {
            $stmt = $db->query($query);

            echo '<table>';
            echo '<thead><tr>';

            for ($i = 0; $i < $stmt->columnCount(); $i++) {
                echo '<th>' . $stmt->getColumnMeta($i)['name'] . '</th>';
            }

            echo '</tr></thead>';

            echo '<tbody>';

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
                echo '<tr>';

                foreach ($row as $cell) {
                    echo '<td>' . $cell . '</td>';
                }

                echo '</tr>';
            }

            echo '</tbody>';
            echo '</table><br />';

        } catch(PDOException $ex) {
            var_dump($ex->getMessage());
        }

        echo "==========================================";
}